/* 
 * SimpleCookie
 * 
 * @author Petar Fedorovsky @ Dimedia http://dimedia.hr
 * plugin for simple manipulation with cookies
 * 
 * Licensed under the MIT licence:
 * http://www.opensource.org/licenses/mit-license.php
 */
var SimpleCookie = function() {
    this.setCookie = function(cname, cvalue, exdays) {
        
        if(exdays !== false){
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString()+"; ";//note trailing space
        }else{
            var expires = "";//session cookie
        }
        
        var domain = document.domain;
        document.cookie = cname + "=" + cvalue + "; " + expires + "domain=" + domain + "; path=/";
        document.cookie = cname + "=" + cvalue + "; " + expires + "domain=" + domain.replace("www", "") + "; path=/";
        document.cookie = cname + "=" + cvalue + "; " + expires + "path=/";
    }
    this.getCookie = function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    this.cookies = null;
    this.getAllCookies = function() {
            var c = document.cookie.split(';');
            var i;
            var tmp = {};
            for (i = 0; i < c.length; i++) {
                var s = c[i].split('=', 2);
                tmp[s[0].trim()] =s[1].trim();
                self.cookies = tmp;

            }

            return self.cookies;

        }
    this.deleteCookie = function eraseCookie(name) {   
        this.setCookie(name,'',0);
    }
        /* Constructor */
        {
            this.getAllCookies();
        }
};